package org.cuongnv.segmentview

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.core.animation.doOnEnd

// Created by cuongnv on 2019-11-28.

class SegmentIndicator : View {
    companion object {
        const val SLIDE_ANIMATION_DURATION = 200L
    }

    private var mDrawableSelected: Drawable? = null

    private var mXOffset: Float = 0f
    private var mYOffset: Float = 0f
    private var mSegmentWidth: Int = 0
    private var mSegmentHeight: Int = 0

    private var mAnimation: ValueAnimator? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)
    constructor(context: Context, attributeSet: AttributeSet, defStyleInt: Int)
            : super(context, attributeSet, defStyleInt)

    fun setSelectedDrawable(drawable: Drawable?) {
        mDrawableSelected = drawable
    }

    fun onSegmentScroll(xOffset: Float, segmentWidth: Int,
                        yOffset: Float, segmentHeight: Int) {
        var changed = false

        if (mSegmentWidth != segmentWidth || mSegmentHeight != segmentHeight) {
            onSegmentBoundChange(segmentWidth, segmentHeight)
            changed = true
        }

        if (mYOffset != yOffset) {
            mYOffset = yOffset
            changed = true
        }

        if (mXOffset != xOffset) {
            if (isInEditMode) {
                mXOffset = xOffset
            } else {
                onSegmentMove(xOffset)
            }
            changed = true
        }

        if (changed) {
            invalidate()
        }
    }

    private fun onSegmentMove(xOffset: Float) {
        doAnimation(xOffset)
    }

    private fun doAnimation(targetOffset: Float) {
        if (mAnimation != null && mAnimation!!.isRunning) {
            mAnimation!!.cancel()
        }

        mAnimation = ValueAnimator.ofFloat(mXOffset, targetOffset)
        mAnimation!!.apply {
            duration = SLIDE_ANIMATION_DURATION
            interpolator = DecelerateInterpolator()
            addUpdateListener {
                mXOffset = it.animatedValue as Float
                invalidate()
            }
            doOnEnd {
                if (mXOffset != targetOffset) {
                    mXOffset = targetOffset
                    invalidate()
                }
            }
        }
        mAnimation!!.start()
    }

    private fun onSegmentBoundChange(width: Int, height: Int) {
        mSegmentWidth = width
        mSegmentHeight = height
        mDrawableSelected?.setBounds(0, 0, width, height)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.let { c ->
            c.save()

            c.translate(mXOffset, mYOffset)
            mDrawableSelected?.draw(c)

            c.restore()
        }
    }
}