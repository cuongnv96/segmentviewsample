package org.cuongnv.segmentview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat

// Created by cuongnv on 2019-11-27.

open class SegmentLayout : RelativeLayout {
    private var mIsFixSize = false

    private lateinit var mSegmentContainer: SegmentContainer
    private val mSegmentIndicator = SegmentIndicator(context)

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        init(context, attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet, defStyleInt: Int)
            : super(context, attributeSet, defStyleInt) {
        init(context, attributeSet)
    }

    private fun parseAttributeSet(context: Context, attrs: AttributeSet) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.SegmentLayout)
        mIsFixSize = typedArray.getBoolean(R.styleable.SegmentLayout_sgm_isFixSize, false)

        val selectedDrawableId =
            typedArray.getResourceId(R.styleable.SegmentLayout_sgm_selectedDrawable, 0)
        if (selectedDrawableId != 0) {
            val selectedDrawable = ContextCompat.getDrawable(context, selectedDrawableId)
            mSegmentIndicator.setSelectedDrawable(selectedDrawable)
        }

        typedArray.recycle()
    }

    private fun init(context: Context, attributeSet: AttributeSet?) {
        attributeSet?.let { parseAttributeSet(context, it) }

        mSegmentContainer =
            if (mIsFixSize) SegmentContainerFix(context) else SegmentContainerScrollable(context)
        mSegmentContainer.setIndicator(mSegmentIndicator)

        addView(
            mSegmentIndicator,
            LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        )
        addView(
            mSegmentContainer as View,
            LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        )
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        if (child is SegmentContainer || child is SegmentIndicator) {
            super.addView(child, index, params)
        } else {
            var childParam = params
            if (childParam != null) {
                childParam = LinearLayout.LayoutParams(childParam as MarginLayoutParams)
            } else {
                childParam = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
                )
                childParam.weight = 1f
            }
            mSegmentContainer.addView(child, index, childParam)
        }
    }

    fun setSegmentChangeListener(l: OnSegmentChange?) {
        mSegmentContainer.setSegmentChangeListener(l)
    }

    interface OnSegmentChange {
        fun onSegmentSelected(segmentId: Int)
        fun onSegmentUnselected(unselectedId: Int)
    }
}