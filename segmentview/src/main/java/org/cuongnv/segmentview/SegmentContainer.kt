package org.cuongnv.segmentview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.HorizontalScrollView
import android.widget.LinearLayout

// Created by cuongnv on 2019-11-28.

interface SegmentContainer {
    fun setIndicator(segmentIndicator: SegmentIndicator)
    fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?)

    fun setSegmentChangeListener(l: SegmentLayout.OnSegmentChange?)

    fun onSegmentSelectedChange()
    fun dispatchSetChildSelected(id: Int, isSelected: Boolean)

    fun getSegmentSelected(): Int
}

class SegmentContainerFix : LinearLayout, SegmentContainer {
    private var mSegmentSelected = 0
    private var mSegmentIndicator: SegmentIndicator? = null
    private var mSegmentChangeListener: SegmentLayout.OnSegmentChange? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)
    constructor(context: Context, attributeSet: AttributeSet, defStyleInt: Int) : super(context, attributeSet, defStyleInt)

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        if (params is LayoutParams) {
            params.weight = 1f
        }

        child?.let { ch ->
            ch.setOnClickListener { v ->
                val newSelected = v.tag as Int
                if (newSelected != mSegmentSelected) {
                    dispatchSetChildSelected(mSegmentSelected, false)
                    mSegmentSelected = newSelected
                    dispatchSetChildSelected(mSegmentSelected, true)
                    onSegmentSelectedChange()
                }
            }
            super.addView(ch, index, params)
            ch.tag = childCount - 1
        }
    }

    override fun setSegmentChangeListener(l: SegmentLayout.OnSegmentChange?) {
        mSegmentChangeListener = l
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        dispatchSetChildSelected(mSegmentSelected, true)
        onSegmentSelectedChange()
    }

    override fun setIndicator(segmentIndicator: SegmentIndicator) {
        mSegmentIndicator = segmentIndicator
    }

    override fun dispatchSetChildSelected(id: Int, isSelected: Boolean) {
        if (id in 0 until childCount) {
            getChildAt(id).isSelected = isSelected

            if (isSelected) {
                mSegmentChangeListener?.onSegmentSelected(id)
            } else {
                mSegmentChangeListener?.onSegmentUnselected(id)
            }
        }
    }

    override fun onSegmentSelectedChange() {
        if (mSegmentSelected in 0 until childCount) {
            val child = getChildAt(mSegmentSelected)
            mSegmentIndicator?.onSegmentScroll(
                    child.left.toFloat(), child.width,
                    child.top.toFloat(), child.height)
        }
    }

    override fun getSegmentSelected(): Int {
        return mSegmentSelected
    }
}

class SegmentContainerScrollable : HorizontalScrollView, SegmentContainer {
    private var mSegmentSelected = 0
    private var mSegmentIndicator: SegmentIndicator? = null
    private val mViewContainer: LinearLayout = LinearLayout(context)
    private var mSegmentChangeListener: SegmentLayout.OnSegmentChange? = null
    private val mChildLocation = IntArray(2)
    private val mIndicatorLocation = IntArray(2).apply { set(0, -1) }

    constructor(context: Context) : super(context)
    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)
    constructor(context: Context, attributeSet: AttributeSet, defStyleInt: Int) : super(context, attributeSet, defStyleInt)

    init {
        isVerticalScrollBarEnabled = false
        isHorizontalScrollBarEnabled = false

        mViewContainer.orientation = LinearLayout.HORIZONTAL
        mViewContainer.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        addView(mViewContainer)
    }

    override fun setIndicator(segmentIndicator: SegmentIndicator) {
        mSegmentIndicator = segmentIndicator
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        if (child is LinearLayout) {
            super.addView(child, index, params)
        } else {
            child?.let { ch ->
                mViewContainer.addView(ch, index, params)
                ch.tag = mViewContainer.childCount - 1
                ch.setOnClickListener { v ->
                    val newSelected = v.tag as Int
                    if (newSelected != mSegmentSelected) {
                        dispatchSetChildSelected(mSegmentSelected, false)
                        mSegmentSelected = newSelected
                        dispatchSetChildSelected(mSegmentSelected, true)
                        onSegmentSelectedChange()
                    }
                }
            }
        }
    }

    override fun setSegmentChangeListener(l: SegmentLayout.OnSegmentChange?) {
        mSegmentChangeListener = l
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        dispatchSetChildSelected(mSegmentSelected, true)
        onSegmentSelectedChange()
    }

    override fun dispatchSetChildSelected(id: Int, isSelected: Boolean) {
        if (id in 0 until mViewContainer.childCount) {
            mViewContainer.getChildAt(id).isSelected = isSelected

            if (isSelected) {
                mSegmentChangeListener?.onSegmentSelected(id)
            } else {
                mSegmentChangeListener?.onSegmentUnselected(id)
            }
        }
    }

    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        super.onScrollChanged(l, t, oldl, oldt)
        if (mSegmentSelected in 0 until mViewContainer.childCount) {
            onSegmentSelectedChange()
        }
    }

    override fun onSegmentSelectedChange() {
        val child = mViewContainer.getChildAt(mSegmentSelected)
        child.getLocationInWindow(mChildLocation)
        mSegmentIndicator?.let {
            val xIndicator = getXIndicator(it)
            it.onSegmentScroll(
                    (mChildLocation[0] - xIndicator).toFloat(), child.width,
                    child.top.toFloat(), child.height)
        }
    }

    private fun getXIndicator(segmentIndicator: SegmentIndicator): Int {
        if (mIndicatorLocation[0] >= 0) return mIndicatorLocation[0]
        segmentIndicator.getLocationInWindow(mIndicatorLocation)
        return mIndicatorLocation[0]
    }

    override fun getSegmentSelected(): Int {
        return mSegmentSelected
    }
}